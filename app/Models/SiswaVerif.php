<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiswaVerif extends Model
{
    use HasFactory;

    protected $table = 'siswa_verifs';
    protected $primaryKey = 'nis';
    protected $guarded = [];
}

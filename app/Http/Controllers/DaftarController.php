<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DaftarController extends Controller
{
    public function siswa_daftar()
    {
        return view('siswa.daftar');
    }

    public function siswa_print($nis)
    {
        $siswa = Siswa::findOrFail($nis);

        return view('siswa.print', compact('siswa'));
    }

    public function siswa_post(Request $request)
    {
        $check = DB::table('siswas')->where('nis', $request->nis)->first();

        $request->validate([
            'nis' => 'required|numeric',
            'nama' => 'required|max:50',
            'email' => 'required|email|unique:siswas',
            'jenkel' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required'
        ]);

        if(!$check)
        {
            Siswa::create($request->all());

            $user = new User;
            $user->name = $request->nama;
            $user->email = $request->email;
            $user->password = bcrypt($request->nis);
            $user->is_admin = 0;
            $user->siswa_id = $request->nis;
            $user->save();

            return redirect()->route('siswa_print', $request->nis);
        }else if($check)
        {
            return redirect()->route('siswa_daftar')->with('error', 'NIS sudah pernah didaftarkan !!!');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\SiswaVerif;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        $siswas = SiswaVerif::latest()->paginate(10);
        return view('adminHome', compact('siswas'));
    }

    public function siswa_edit($nis)
    {
        $siswa = Siswa::findOrFail($nis);

        return view('siswa.edit', compact('siswa'));
    }

    public function siswa_update(Request $request, $nis)
    {
        $siswa = Siswa::findOrFail($nis);
        $request->validate([
            'nama' => 'required|max:50',
            'jenkel' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required'
        ]);

        $siswa->update($request->all());

        return redirect()->route('home')->with('success', 'Data berhasil diperbaharui');
    }

    public function siswa_verif(Request $request, $id)
    {
        $check = DB::table('siswa_verifs')->where('nis', $request->nis)->first();

        $this->validate($request, [
            'nis' => 'required|numeric',
            'nama' => 'required|max:50',
            'email' => 'required|email',
            'jenkel' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required'
        ]);

        if(!$check)
        {
            SiswaVerif::create($request->all());

            $user = User::findOrFail($id);
            $user->update([
                'verif_id' => $request->nis
            ]);

            return redirect()->route('home')->with('success', 'Data berhasil diverifikasi');
        }else if($check)
        {
            return redirect()->route('home')->with('error', 'Data sudah pernah diverifikasi !!!');
        }
    }

    public function siswa_delete($nis)
    {
        $verif = SiswaVerif::findOrFail($nis);
        $verif->delete();

        return redirect()->route('admin.home')->with('success', 'Data berhasil dihapus');
    }
}

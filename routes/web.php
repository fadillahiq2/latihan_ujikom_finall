<?php

use App\Http\Controllers\DaftarController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('landing');

Route::get('/siswa/daftar', [DaftarController::class, 'siswa_daftar'])->name('siswa_daftar');
Route::post('/siswa/daftarPost', [DaftarController::class, 'siswa_post'])->name('siswa_post');
Route::get('/siswa/print/{nis}', [DaftarController::class, 'siswa_print'])->name('siswa_print');

Auth::routes([
    'register' => false,
    'verify' => false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/edit/{nis}', [App\Http\Controllers\HomeController::class, 'siswa_edit'])->name('siswa_edit');
Route::put('/home/Update/{nis}', [App\Http\Controllers\HomeController::class, 'siswa_update'])->name('siswa_update');
Route::post('/home/verif/{id}', [App\Http\Controllers\HomeController::class, 'siswa_verif'])->name('siswa_verif');

Route::get('/admin', [App\Http\Controllers\HomeController::class, 'admin'])->middleware('is_admin')->name('admin.home');
Route::delete('/admin/delete/{nis}', [HomeController::class, 'siswa_delete'])->middleware('is_admin')->name('siswa_delete');

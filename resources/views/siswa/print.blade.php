
<br>

<body style="font-family: sans-serif" onload="window.print()">
<center>
    <table width="94%" border="0">
        <tr>
            <td width="10%">
                <center><div><img src="{{ asset('assets/img/logo-wk.png') }}" width="100px"></div></center>
            </td>
            <td>
                <b>PANITIA PENERIMAAN PESERTA DIDIK BARU</b><br>
                <b>SMK WIKRAMA BOGOR T.P. 2020-2021</b><br>
                Jl. Raya Wangun Kel. Sindangsari Kec. Bogor Timur Kota Bogor <br>
                Email : prohumasi@smkwikrama.sch.id
            </td>
        </tr>

    </table>
</center>
<br>
    <center><b>TANDA BUKTI PENDAFTARAN</b></center>
    <br>
    <table width="55%" border="0" style="margin-left:3%;margin-right:2%;float:left">
        <tr>
            <td colspan="3" style="background-color: lightgray"><center><b>BIODATA CALON PESERTA DIDIK</b></center></td>
        </tr>
        <tr>
            <td width="30%"><b>TANGGAL DAFTAR</b></td>
            <td width="3%">:</td>
            <td>{{ Carbon\Carbon::parse($siswa->created_at)->IsoFormat('D MMM Y') }}</td>
        </tr>
        <tr>
            <td width="30%"><b>NAMA LENGKAP</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->nama}}</td>
        </tr>
        <tr>
            <td width="30%"><b>JENIS KELAMIN</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->jenkel}}</td>
        </tr>
        <tr>
            <td width="30%"><b>NIS</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->nis}}</td>
        </tr>
        <tr>
            <td width="30%"><b>Email</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->email}}</td>
        </tr>
        <tr>
            <td width="30%"><b>TEMPAT LAHIR</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->temp_lahir}}</td>
        </tr>
        <tr>
            <td width="30%"><b>TANGGAL LAHIR</b></td>
            <td width="3%">:</td>
            <td>{{ Carbon\Carbon::parse($siswa->tgl_lahir)->IsoFormat('D MMM Y') }}</td>
        </tr>
        <tr>
            <td width="30%"><b>ALAMAT</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->alamat}}</td>
        </tr>
        <tr>
            <td width="30%"><b>ASAL SEKOLAH</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->asal_sekolah}}</td>
        </tr>
        <tr>
            <td width="30%"><b>KELAS</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->kelas}}</td>
        </tr>
        <tr>
            <td width="30%"><b>JURUSAN</b></td>
            <td width="3%">:</td>
            <td>{{$siswa->jurusan}}</td>
        </tr>
    </table>
    <table width="37%" border="0">
        <tr>
            <td style="background-color: lightgray"><center><b>INFORMASI DAN PERSYARATAN</b></center></td>
        </tr>
    </table>
    <br>
    <table width="37%" border="0">
        <tr>
            <td><b>A. Foto/Scan Dokumen yang Harus Dipersiapkan</b></td>
        </tr>

        <tr>
            <td>
                1. Raport Semester 1 s.d 5 yang telah dilegasir dalam bentuk pdf(satu file)<br>
                2. Akta Kelahiran dalam bentuk pdf/jpeg(satu file)<br>
                3. KTP Ayah dan ibu dipisah dalam bentuk pdf/jpeg<br>
                4. Kartu Keluarga dalam bentuk pdf/jpeg<br>
                5. Kartu NISN dalam bentuk pdf/jpeg
            </td>
        </tr>

    </table>
    <br>
    <table width="37%" border="0">
        <tr>
            <td style="background-color: white"><b>B. Akun</b></td>
        </tr>
        <tr>
            <td>Gunakan akun ini untuk login</td>
        </tr>
        <tr>
            <td>Email : {{$siswa->email}}</td>
        </tr>
        <tr>
            <td>
                Password : {{$siswa->nis}}
            </td>
        </tr>
        <tr>
            <td>Login  : <a href="{{ route('login') }}">Klik Disini</a></td>
        </tr>
        <tr>
            <td>Akun ini digunakan untuk mengecek status pendaftaran pada SIM PPDB SMK Wikrama</td>
        </tr>

    </table>
</body>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <tr>
                            <th>NIS</th>
                            <th>Email</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Asal Sekolah</th>
                            <th>Kelas</th>
                            <th>Jurusan</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($siswas as $siswa)
                            <tr>
                                <td>{{ $siswa->nis }}</td>
                                <td>{{ $siswa->email }}</td>
                                <td>{{ $siswa->nama }}</td>
                                <td>{{ $siswa->jenkel }}</td>
                                <td>{{ $siswa->temp_lahir }}</td>
                                <td>{{ Carbon\Carbon::parse($siswa->nis)->IsoFormat('D MMM Y') }}</td>
                                <td>{{ $siswa->alamat }}</td>
                                <td>{{ $siswa->asal_sekolah }}</td>
                                <td>{{ $siswa->kelas }}</td>
                                <td>{{ $siswa->jurusan }}</td>
                                <td>
                                    <form action="{{ route('siswa_delete', $siswa->nis) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    {!! $siswas->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
